package org.alostale.testconn;

public class Response {
  int code;
  String body;

  public Response(int code, String body) {
    this.code = code;
    this.body = body;
  }
}
