package org.alostale.testconn;

public interface RequestHandler {
  Response get(String url) throws Exception;
}
