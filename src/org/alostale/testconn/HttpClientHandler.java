package org.alostale.testconn;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;

public class HttpClientHandler implements RequestHandler {
  private static HttpClient sharedClient = HttpClient.newBuilder()
      .connectTimeout(Duration.ofSeconds(30))
      .build();

  @Override
  public Response get(String url) throws Exception {

    HttpClient client;
    if (!true) {
      // Don't do this: httpclient should be shared
      HttpClient c = HttpClient.newBuilder().connectTimeout(Duration.ofSeconds(30)).build();
      client = c;
    } else {
      client = sharedClient;
    }

    HttpRequest request = HttpRequest.newBuilder().uri(new URI(url)).GET().build();

    HttpResponse<String> resp = client.send(request, BodyHandlers.ofString());
    return new Response(resp.statusCode(), resp.body());
  }

}
