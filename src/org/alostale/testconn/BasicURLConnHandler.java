package org.alostale.testconn;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class BasicURLConnHandler implements RequestHandler {

  @Override
  public Response get(String url) throws Exception {
    HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
    con.setConnectTimeout(30_000);
    con.setReadTimeout(30_000);
    con.setRequestMethod("GET");

    con.setDoOutput(true);
    con.setDoInput(true);

    int code = con.getResponseCode();

    InputStream inputStream = con.getInputStream();
    BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
    String inputLine;
    StringBuffer respBody = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
      respBody.append(inputLine);
    }
    in.close();

    con.disconnect();
    return new Response(code, respBody.toString());
  }

}
