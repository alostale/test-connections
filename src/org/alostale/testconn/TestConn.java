package org.alostale.testconn;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TestConn {

  public static void main(String[] args) throws Exception {
    String url = "http://localhost:8000/";

    int waitSec = 10;
    System.out.println("Waiting " + waitSec + " secs to start");
    Thread.sleep(waitSec * 1000);
    System.out.println("Starting...");

    long t = System.currentTimeMillis();

    ExecutorService threadPool = Executors.newFixedThreadPool(20);

    for (int i = 0; i < 25_000; i++) {
      int n = i;
      threadPool.submit(() -> {
        RequestHandler rh;

        if (true) {
          rh = new HttpClientHandler();
        } else {
          rh = new BasicURLConnHandler();
        }
        Response resp;
        try {
          resp = rh.get(url);
          if (resp.code != 200 || n % 10 == 0) {
            System.out.println(((System.currentTimeMillis() - t)) + "-" + n + "-" + resp.code);
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      });
    }

    threadPool.shutdown();
    threadPool.awaitTermination(60, TimeUnit.SECONDS);
    System.out.println("Done " + (System.currentTimeMillis() - t) + "ms");
  }

}
